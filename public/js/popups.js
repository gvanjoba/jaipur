function show_message(txt , time=1){
    document.getElementById("message").innerHTML = txt;
    document.getElementById('popup-message').style.display = "block";
    window.setTimeout(function(){
        document.getElementById('popup-message').style.display = "None";
    }, time*1000);
}

function show_instructions(){
    let instr = `
    <h1>Jaipur</h1> 
    <a href="https://www.yucata.de/en/Rules/Jaipur" target="_blank"> real jaipur rules </a>
        <!-- this is not mine, it is copied from another site, 
        so i didn't t ake the styles to a seperate page, although i changed the text to match my needs --!>
        <h2>Introduction and Aim of the Game</h2>
        <p style="text-align:justify">
          Jaipur... You are hoping to become the Maharaja's personal trader by beeing richer than your opponent at the end of each
          week (round). To do so, collect and exchange goods (cards) at the market, then sell them for rupees(prices in the left). 
          no matter how many you sell the price will be the same till next turn.
          <br> As for the camels, they are mainly used for exchanges when you want to take multiple goods at the market.
          <br>
          the game goes on untill one of the players reach 80
        </p>

        <h2>Material</h2>
        <table style="background-color:white; width:830px;padding:15px;border-radius:8px;">
          <tbody>
            <tr>
              <td colspan="4">
                <strong>55 goods- and camel cards</strong>
                <br>&nbsp;</td>
            </tr>
            <tr>
              <td>6x diamonds</td>
              <td>6x gold</td>
              <td>6x silver</td>
              <td>8x cloth</td>
            </tr>
            <tr>
              <td>
                <img alt="diamonds" title="diamonds" src="/src/cards/diamonds.png">
              </td>
              <td>
                <img alt="gold" title="gold" src="/src/cards/gold.png">
              </td>
              <td>
                <img alt="silver" title="silver" src="/src/cards/silver.png">
              </td>
              <td>
                <img alt="cloth" title="cloth" src="/src/cards/cloth.png">
              </td>
            </tr>
            <tr>
              <td style="padding-top: 15px;">8x spice</td>
              <td style="padding-top: 15px;">10x leather</td>
              <td style="padding-top: 15px;">11x camels</td>
              <td style="padding-top: 15px;">back</td>
            </tr>
            <tr>
              <td>
                <img alt="spice" title="spice" src="/src/cards/spice.png">
              </td>
              <td>
                <img alt="leather" title="leather" src="/src/cards/leather.png">
              </td>
              <td>
                <img alt="camel" title="camel" src="/src/cards/camel.png">
              </td>
              <td>
                <img alt="back" title="back" src="/src/card_back.png">
              </td>
            </tr>
          </tbody>
        </table>
        <br>

        <h2>Game Turn</h2>
        <p style="text-align:center">
          On your turn, you can either:
          <br>
          <br>
          <strong>
            <span style="background-color:#9C2994; border-radius:5px; padding:10px;color:White;">TAKE CARDS</span>
            <span style="padding:10px;"> OR </span>
            <span style="background-color:#844D18; border-radius:5px; padding:10px;color:White;">SELL CARDS</span>
          </strong>
          <br>
          <br>
          <em>But never both!</em>
          <br>
          <br> Your turn is now over and your opponent chooses one of these actions.
        </p>
        <h2 style="border-color:#9C2994; border-style:solid;border-width:2px;width:830px;">Take cards</h2>
        <p style="text-align:justify;">
          If you take cards, you must choose one of the following options:
        </p>
        <ul>
          <li>A) take
            <span style="color:red" ;="">several</span> goods (
            <span style="color:red" ;="">=Exchange!</span>)</li>
          <li>B) take
            <span style="color:red" ;="">1 single</span> good</li>
          <li>C) take
            <span style="color:red" ;="">all</span> the camels.</li>
        </ul>
        <p></p>
        <h2 style="border-color:#9C2994; border-style:solid;border-width:2px;width:830px;">A - Take several goods</h2>
        <p style="text-align:justify;">
          Take all the goods cards that you want into your hand (they can be of different types), then exchange
          <span style="color:red" ;="">the same number</span> of cards. The returned cards can be
          <strong>camels, goods,</strong>
          or a
          <strong>combination of the two</strong>.
        </p>
        <h2 style="border-color:#9C2994; border-style:solid;border-width:2px;width:830px;">B - Take 1 single good</h2>
        <p style="text-align:justify;">
          Take
          <span style="color:red" ;="">a single</span> goods card from the market into your hand, then replace it with the top card from the deck.
          <br>
          <span style="color:red" ;="">Attention: Players may never have more than 7 cards in their hand.</span>
        </p>
        <h2 style="border-color:#9C2994; border-style:solid;border-width:2px;width:830px;">C - Take the camels</h2>
        <p style="text-align:justify;">
          Take
          <span style="color:red" ;="">all</span> camels from the market and add them to your herd, then replace the cards taken by drawing from the deck.
        </p>
        <h2 style="border-color:#844D18; border-style:solid;border-width:2px;width:830px;">Sell cards</h2>
        <p style="text-align:justify;">
          To sell cards, choose one type of good and discard as many cards of that type as you like onto the discard pile. Each sale
          earns price according to price table;
          <br>
        </p>`;
    document.getElementById("popup-main").innerHTML = instr;
    document.getElementById('popup-instructions').style.display = "block";
}

function hide_popup(){
    document.getElementById('popup-instructions').style.display = "None";
}