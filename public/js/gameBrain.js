

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function load_deck(cards) {
    card_data = cards;
    card_data.deck = [];
    card_data.forEach(card => {
        for (let i = 0; i < card.count; i++) {
            card_data.deck.push(card.content);
        }
    });
}

function fill_prices() {
    card_data.forEach(elem => {
        if (elem.content.name != "camel")
            prices[elem.content.name] = { "price": elem.s_price, "img": elem.content.img };
    });
}

function new_game() {

    let name1 = document.getElementById("player-1").value;
    let name2 = document.getElementById("player-2").value;

    if (name1 == '' || name2 == '') {
        show_message("enter both player names");
        return;
    }
    switch_content();
    let copy = card_data.deck.slice();

    let tb_dec = [];
    for (let i = 0; i < 3; i++) tb_dec.push(copy.pop());

    cur_deck = shuffle(copy);
    fill_prices();
    render_prices();

    opp = new Player(name1, deal(5), document.getElementById("opponent"), opponent_card_clicked, opponent_option_clicked);
    plr = new Player(name2, deal(5), document.getElementById("player"), player_card_clicked, player_option_clicked);

    deal(2).forEach(card => {tb_dec.push(card)});
    table = new Table(tb_dec, document.getElementById("game-table-cards"), table_card_clicked);

    cur = plr;
}

function switch_turn() {
    if (cur.score >= WIN_SCORE) {
        show_message(cur.name + " won the round", 3);
        switch_content(true);
        return;
    }
    if (cur === opp) {
        cur = plr;
    } else {
        cur = opp;
    }
}

function update_data() {
    switch (cur.mode) {
        case cur.options1[0]: return f_exchange();
        case cur.options1[1]: return f_from_deck();
        case cur.options1[2]: return f_take_camels();
        case cur.options1[3]: return f_sell_cards();
    }
    return false;
}

function update_price(data) {
    prices[data.name].price -= data.count;
    if (prices[data.name].price < 1) prices[data.name].price = 1;
}

function f_exchange() {
    if (!table.selected) {
        show_message("select at least one card from table");
        return false;
    }

    if ((table.selected.length > cur.herd.length && !cur.selected) ||
        (cur.selected && table.selected.length > (cur.herd.length + cur.selected.arr.length))) {
        show_message("camels and selected cards in hand are less than the selected cards on the table", 3);
        return false;
    }
    if (cur.selected && cur.selected.arr.length > table.selected.length) {
        show_message("selected cards in hand are more than the selected cards on the table", 3);
        return false;
    }

    let count = cur.hand.length + table.selected.length;
    if (cur.selected) count -= cur.selected.arr.length;
    if (count > MAX_LENGTH) {
        show_message("maximum number of cards in hand is " + MAX_LENGTH, 2);
        return false;
    }

    table.selected.forEach(ind => {
        let tmp = table.board[ind];
        if (cur.selected && cur.selected.arr.length > 0) {
            let i = cur.selected.arr.splice(0, 1);
            table.board[ind] = cur.hand[i];
            cur.hand[i] = tmp;
        } else {
            table.board[ind] = cur.herd.splice(0, 1)[0];
            cur.hand.push(tmp);
        }
    });
    return true;
}

function f_from_deck() {
    if (cur.hand.length < MAX_LENGTH) {
        let card = deal(1)[0];
        if (card.name == 'camel') cur.herd.push(card);
        else cur.hand.push(card);
        return true;
    }
    show_message("maximum number of cards in hand is " + MAX_LENGTH);
    return false;
}

function f_take_camels() {

    let herd = table.board.filter(function (data) { return data.name == 'camel' });
    if (herd.length > 0) {
        table.board = table.board.filter(function (data) { return data.name != 'camel' });
        for (let i in herd) {
            table.board.push(deal(1)[0]);
            cur.herd.push(herd[i]);
        }
        return true;
    }
    show_message("there are no camels on the table");
    return false;
}

function f_sell_cards() {
    if (!cur.selected) {
        show_message("you must select at least one card to sell");
        return false;
    }
    cur.selected.arr.sort().reverse().forEach(ind => {
        cur.score += prices[cur.hand[ind].name].price;
        cur.hand.splice(ind, 1);
    });
    update_price({ "name": cur.selected.name, "count": cur.selected.arr.length });
    render_prices();
    return true;
}

function deal(num_cards) {
    let hand = [];
    for (let i = 0; i < num_cards; i++) {
        hand.push(cur_deck.pop());
    }
    return hand;
}
