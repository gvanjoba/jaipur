
document.addEventListener('DOMContentLoaded', function() {

    fetch('DB/game.json').then(function(response) {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }
        response.json().then(function(data){
            load_deck(data.cards);
        });

        document.getElementById("new-game").addEventListener("click", new_game);
        document.getElementById("menu").addEventListener("click", switch_content);
         
        document.getElementById("instructions").addEventListener("click", show_instructions);
        document.getElementsByClassName("close")[0].addEventListener("click", hide_popup);

    })
    .catch(function(err) {
            //
    });


});