var card_data = {};
var cur_deck = [];
var prices = {};
var opp, plr, cur, table;

const MAX_LENGTH = 7;

const WIN_SCORE = 50;

function switch_content(game){
    document.getElementById("starter-page").style.display = game ? 'block' : 'none';
    document.getElementById("content").style.display = game ? 'none' : 'block';
    document.getElementById("prices").innerHTML = ``;
}

function opponent_card_clicked() {
    if (cur !== opp) show_message("not your card");
    else {
        card_clicked(this.dataset);
    }
}


function player_card_clicked() {
    if (cur !== plr) show_message("not your card");
    else {
        card_clicked(this.dataset);
    }
}


function opponent_option_clicked() {
    if (cur !== opp) show_message("not your turn");
    else {
        option_clicked(this.dataset);
    }
}


function player_option_clicked() {
    if (cur !== plr) show_message("not your turn");
    else {
        option_clicked(this.dataset);
    }
}


function table_card_clicked() {
    if (!cur.mode) {
        show_message("select play mode");
        return;
    }
    if (cur.mode != cur.options1[0]){
        show_message("selection from table is not allowed in " + cur.mode);
        return;
    }
    if (this.dataset.name == 'camel'){
        show_message("to take camels press 'take camels' button");
        return;
    }
    table.select(this.dataset.ind);
}

function card_clicked(data) {
    if (!cur.mode) {
        show_message("select play mode");
        return;
    }
    if (cur.selected && cur.selected.name !== data.name && cur.mode != cur.options1[0]) {
        show_message("select only " + cur.selected.name + " cards");
        return;
    }
    cur.select(data.ind);
}

function option_clicked(data) {
    if (cur.mode) {
        if (data.mode == 'finish') {
            if (update_data()) {
                cur.finish_mode();
                table.refresh();
                switch_turn();
            }
        } else {
            cur.finish_mode();
            table.refresh();
        }
        
    } else {
        cur.set_mode(data.mode);
        if (data.mode == cur.options1[1] || data.mode == cur.options1[2]) {
            if (update_data()) {
                cur.finish_mode();
                table.refresh();
                switch_turn();
            } else {
                cur.finish_mode();
            }
        }
    }
}
