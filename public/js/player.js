
class Player {
    constructor(name, hand, elem, card_clicked_func, option_clicked_func) {
        this.name = name;
        this.hand = hand.filter(function(data){return data.name != 'camel'});
        this.herd = hand.filter(function(data){return data.name == 'camel'});
        this.elem = elem;
        this.options1 = ["exchange", "from deck", "take camels", "sell cards"];
        this.options2 = ["finish", "cancel"];
        this.score = 0;
        this.mode = undefined;
        this.selected = undefined;
        this.card_clicked_func = card_clicked_func;
        this.option_clicked_func = option_clicked_func;
        this.draw();
    };

    draw() {
        this.elem.getElementsByClassName("player-name")[0].innerHTML = this.name;
        this.draw_options(this.options1);
        this.redraw_score();
        this.redraw();
    };

    draw_options(opt) {
        let options = this.elem.getElementsByClassName("play-options")[0];
        options.innerHTML = ``;
        for (let o in opt){
            options.innerHTML +=  `<div class="button option-button" data-mode="${opt[o]}">${opt[o]}</div>`;
        }
        set_click_fn(options, 'div', this.option_clicked_func);
    };

    redraw() {
        this.elem.getElementsByClassName("game-cards")[0].innerHTML = render_cards(this.hand);
        this.elem.getElementsByClassName("herd")[0].innerHTML = render_herd(this.herd);
        set_click_fn(this.elem.getElementsByClassName("game-cards")[0], 'img', this.card_clicked_func);
    };  

    swap(c1, c2) {
        if (c1.length !== c2.length) {
            return 1;
        }
        return 0;
        this.redraw();
    };

    select(ind) {
        let card = this.hand[ind];
        if (!this.selected) {
            this.selected = { "name": card.name, "arr": [] }
        }
        let index = this.selected.arr.indexOf(ind);
        if (index == -1) {
            this.elem.getElementsByTagName("img")[ind].className = "game-card selected";
            this.selected.arr.push(ind);
        } else {
            this.elem.getElementsByTagName("img")[ind].className = "game-card normal";
            this.selected.arr.splice(index, 1);
            if (this.selected.arr.length == 0) this.selected = undefined;
        }
    };

    set_mode(mode){
        this.mode = mode;
        this.draw_options(this.options2);
    }

    finish_mode(){
        this.mode = undefined;
        this.draw_options(this.options1);
        this.redraw();
        this.redraw_score();
        this.selected = undefined;
    }

    redraw_score(){
        this.elem.getElementsByClassName("player-score")[0].innerHTML = this.score;
    }
};