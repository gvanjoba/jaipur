function render_cards(cards) {
    let tmp = ``;
    for (let i = 0; i < cards.length; i++) {
        tmp += render_card(cards[i], i);
    }
    return tmp;
}

function render_card(card, ind) {
    return `<img class="game-card normal" data-name="${card.name}"  data-ind="${ind}" src="${card.img}"/>`;
}

function render_herd(herd) {
    if(herd.length == 0) return ``;
    tmp = `<div class="herd-count"><div> x${herd.length}</div></div>`;
    return tmp;
}

function set_click_fn(cards_elem, tag, click_fn) {
    let cards_arr = cards_elem.getElementsByTagName(tag);

        for (let i = 0; i < cards_arr.length; i++) {
            cards_arr[i].addEventListener("click", click_fn);
        }
}

function render_prices(){
    let tmp = `<h1> PRICES </h1>`;
    for (const name in prices){
        let card = prices[name];
        
        tmp += `<div class="card-score-div"><img class="game-card small" src="${card.img}"/>
                <div class="card-score"> ${card.price}</div></div>`;
    }
    document.getElementById("prices").innerHTML = tmp;
}