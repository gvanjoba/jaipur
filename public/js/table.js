class Table {
    constructor( board, elem, card_clicked_func) {
        this.board = board;
        this.elem = elem;
        this.selected = undefined;
        this.card_clicked_func = card_clicked_func;
        this.redraw_cards();
    };

    redraw_cards() {

        this.elem.innerHTML = render_cards(this.board);
        
        set_click_fn(this.elem, 'img', this.card_clicked_func);
    };  

    refresh() {
        this.selected = undefined;
        this.redraw_cards();
    }

    select(ind) {
        let card = this.board[ind];
        if (!this.selected) {
            this.selected = [];
        }
        let index = this.selected.indexOf(ind);
        if (index == -1) {
            this.elem.getElementsByTagName("img")[ind].className = "game-card selected";
            this.selected.push(ind);
        } else {
            this.elem.getElementsByTagName("img")[ind].className = "game-card normal";
            this.selected.splice(index, 1);
            if (this.selected.length == 0) this.selected = undefined;
        }
    }
};